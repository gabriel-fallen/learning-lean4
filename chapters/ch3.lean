namespace props_as_types

section silly

variable {p : Prop} {q : Prop}

theorem t1 : p -> q -> p := λ hp => λ hq => hp

end silly

section trivial

variable (p q : Prop)

#check p -> q -> p ∧ q
#check ¬p ↔ p -> False

example (h : p ∧ q) : q ∧ p := ⟨h.right, h.left⟩
example (h : p ∧ q) : q ∧ p ∧ q := ⟨h.right, h.left, h.right⟩

example (h : p ∨ q) : q ∨ p :=
  h.elim (λ hp => Or.inr hp) (λ hq => Or.inl hq)

example (hpq : p -> q) (hnq : ¬q) : ¬p :=
  λ hp : p =>
  show False from hnq (hpq hp)

example (r : Prop) (hnp : ¬p) (hq : q) (hqp : q -> p) : r :=
  absurd (hqp hq) hnp

end trivial

section exercises

variable (p q r : Prop)

-- commutativity of ∧ and ∨
example : p ∧ q ↔ q ∧ p :=
  ⟨λ h => ⟨h.right, h.left⟩, λ h => ⟨h.right, h.left⟩⟩
example : p ∨ q ↔ q ∨ p :=
  ⟨ λ h => h.elim (λ hp => Or.inr hp) (λ hq => Or.inl hq)
  , λ h => h.elim (λ hq => Or.inr hq) (λ hp => Or.inl hp)⟩

-- associativity of ∧ and ∨
example : (p ∧ q) ∧ r ↔ p ∧ (q ∧ r) := sorry
example : (p ∨ q) ∨ r ↔ p ∨ (q ∨ r) := sorry

-- distributivity
example : p ∧ (q ∨ r) ↔ (p ∧ q) ∨ (p ∧ r) :=
  let pqr := p ∧ (q ∨ r)
  let pqpr := (p ∧ q) ∨ (p ∧ r)
  have lr : pqr -> pqpr :=
    λ ⟨hp, hqr⟩ => hqr.elim ((λ hq => Or.inl ⟨hp, hq⟩)) (λ hr => Or.inr ⟨hp, hr⟩)
  have rl : pqpr -> pqr :=
    λ h => h.elim (λ ⟨hp, hq⟩ => ⟨hp, Or.inl hq⟩) (λ ⟨hp, hr⟩ => ⟨hp, Or.inr hr⟩)
  ⟨lr, rl⟩
example : p ∨ (q ∧ r) ↔ (p ∨ q) ∧ (p ∨ r) := sorry

-- other properties
example : (p → (q → r)) ↔ (p ∧ q → r) := sorry
example : ((p ∨ q) → r) ↔ (p → r) ∧ (q → r) := sorry
example : ¬(p ∨ q) ↔ ¬p ∧ ¬q := sorry
example : ¬p ∨ ¬q → ¬(p ∧ q) := sorry
example : ¬(p ∧ ¬p) := sorry
example : p ∧ ¬q → ¬(p → q) := sorry
example : ¬p → (p → q) := sorry
example : (¬p ∨ q) → (p → q) := sorry
example : p ∨ False ↔ p := sorry
example : p ∧ False ↔ False := sorry
example : (p → q) → (¬q → ¬p) := sorry

end exercises

namespace exercises_classical

open Classical

variable (p q r s : Prop)

example : (p → r ∨ s) → ((p → r) ∨ (p → s)) := sorry
example : ¬(p ∧ q) → ¬p ∨ ¬q := sorry
example : ¬(p → q) → p ∧ ¬q := sorry
example : (p → q) → (¬p ∨ q) := sorry
example : (¬q → ¬p) → (p → q) := sorry
example : p ∨ ¬p := sorry
example : (((p → q) → p) → p) := sorry

end exercises_classical

end props_as_types
