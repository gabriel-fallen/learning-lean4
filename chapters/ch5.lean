namespace tactics

example (p q : Nat -> Prop) : (∃ x, p x) -> ∃ x, p x ∨ q x := by
  intro h
  cases h with
  | intro w hw => constructor; exact Or.inl hw

example (p q : Nat -> Prop) : (∃ x, p x) -> ∃ x, p x ∨ q x := by
  intro h
  cases h with
  | intro w hw => exists w; exact Or.inl hw

example (p q : Nat -> Prop) : (∃ x, p x ∧ q x) -> ∃ x, q x ∧ p x := by
  intro h
  cases h with
  | intro x hpq =>
    cases hpq with
    | intro hp hq =>
      exists x
      constructor <;> assumption

example (p q r : Prop) : p ∧ (q ∨ r) <-> p ∧ q ∨ p ∧ r := by
  apply Iff.intro
  . intro
    | ⟨hp, Or.inl hq⟩ => apply Or.inl; constructor <;> assumption
    | ⟨hp, Or.inr hr⟩ => apply Or.inr; constructor <;> assumption
  . intro
    | Or.inl ⟨hp, hq⟩ => constructor; assumption; exact Or.inl hq
    | Or.inr ⟨hp, hr⟩ => constructor; assumption; exact Or.inr hr

example (f : Nat -> Nat) (a : Nat) (h : a + 0 = 0) : f a = f 0 := by
  rw [Nat.add_zero] at h
  rw [h]


section simplifier

open List

example (xs : List Nat) : reverse (xs ++ [1, 2, 3]) = [3, 2, 1] ++ reverse xs :=
  by simp

example (xs ys : List α) : length (reverse (xs ++ ys)) = length xs + length ys :=
  by simp [Nat.add_comm]

attribute [local simp] Nat.mul_comm Nat.mul_assoc Nat.mul_left_comm
attribute [local simp] Nat.add_assoc Nat.add_comm Nat.add_left_comm

example (w x y z : Nat) (p : Nat -> Prop)
        (h : p (x * y + z * w  * x)) : p (x * w * z + y * x) := by
  simp at *; assumption

example (w x y z : Nat) (p : Nat -> Prop)
        (h1 : p (1 * x + y)) (h2 : p  (x * z * 1))
        : p (y + 0 + x) ∧ p (z * x) := by
  simp at *; constructor <;> assumption

example (u w x y z : Nat) (h1 : x = y + z) (h2 : w = u + x)
        : w = z + y + u := by
  simp [*] -- because [local simp]s


def mk_symm (xs : List α) := xs ++ xs.reverse

theorem reverse_mk_symm (xs : List α)
        : (mk_symm xs).reverse = mk_symm xs := by
  simp [mk_symm]

example (xs ys : List α)
        : (xs ++ mk_symm ys).reverse = mk_symm ys ++ xs.reverse := by
  simp [reverse_mk_symm]

example (xs ys : List α) (p : List α -> Prop)
        (h : p (xs ++ mk_symm ys).reverse)
        : p (mk_symm ys ++ xs.reverse) := by
  simp [reverse_mk_symm] at h; assumption

example : 0 < 1 + x ∧ x + y + 2 ≥ y + 1 := by
  simp_arith

end simplifier

section split

def f (x y z : Nat) : Nat :=
  match x, y, z with
  | 5, _, _ => y
  | _, 5, _ => y
  | _, _, 5 => y
  | _, _, _ => 1

example (x y z : Nat) : x != 5 → y != 5 → z != 5 -> z = w -> f x y w = 1 := by
  intros
  simp [f]
  split
  . contradiction
  . contradiction
  . simp [*] at *
  . rfl

def g (xs ys : List Nat) : Nat :=
  match xs, ys with
  | [a, b], _ => a+b+1
  | _, [b, c] => b+1
  | _, _      => 1

example (xs ys : List Nat) (h : g xs ys = 0) : False := by
  simp [g] at h
  split at h <;> simp_arith at h

end split

section extensible

-- Define a new tactic notation
syntax "triv" : tactic

macro_rules
  | `(tactic| triv) => `(tactic| assumption)

example (h : p) : p := by
  triv

-- Let's extend `triv`. The tactic interpreter
-- tries all possible macro extensions for `triv` until one succeeds
macro_rules
  | `(tactic| triv) => `(tactic| rfl)

example (x : α) : x = x := by
  triv

example (x : α) (h : p) : x = x ∧ p := by
  apply And.intro <;> triv

-- We now add a (recursive) extension
macro_rules
  | `(tactic| triv) => `(tactic| apply And.intro <;> triv)

example (x : α) (h : p) : x = x ∧ p := by
  triv

end extensible

example (p q r : Prop) (hp : p)
        : (p ∨ q ∨ r) ∧ (q ∨ p ∨ r) ∧ (q ∨ r ∨ p) := by
  simp [*]

end tactics
