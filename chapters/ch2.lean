namespace intro

def F.{u} (α : Type u) : Type u := Prod α α

#check F

def greater (x y : Nat) : Nat :=
  if x > y
  then x
  else y

def doTwice (f : Nat -> Nat) (x : Nat) : Nat := f (f x)

#eval doTwice (λ x => x + x) 3

end intro

section vars

variable (α β γ : Type)
variable (f : α -> β) (g : β -> γ) (h : α -> α)
variable (x : α)

def compose := g (f x)
def doTwice := h (h x)
def doThrice := h (h (h x))

#print compose
#print doTwice
#print doThrice

end vars
